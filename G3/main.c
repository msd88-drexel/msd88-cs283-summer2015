//
//  main.c
//  A2
//
//  Created by Matthew D'Amore on 8/2/15.
//  Copyright (c) 2015 derp. All rights reserved.
//

#include <stdio.h>
#include <ftw.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>

#define path "/Users/Matt/Desktop/files"

static int display_info(const char *fpath, const struct stat *sb,  int tflag, struct FTW *ftwbuf);
void  findAndReplaceFile(char * string, char * replaceString , const char * fileName);
char * Replace(char * line);

char * searchString;
char * replaceString;
char * fileName;

char * Replace(char * line){

    char * newLine = malloc(sizeof(char) * strlen(line));
    char * beginRep = strcasestr(line, searchString);
    char * tracker = line;
    
    while(tracker != beginRep){
//        puts("copying first part of string to newline...");
        strncat(newLine, tracker, 1);
        tracker++;
    }
//    tracker++;
    
    printf("First part successfully copied! current newLine status: %s\n", newLine);
    
    strcat(newLine, replaceString);
    
    printf("incrementing the char pointer by the length of the replace string: %s\n", tracker);
    
    tracker += strlen(searchString);
    strcat(newLine, tracker);
    
    printf("tracker incremented successfully: %s\n", tracker);
    
    printf("The new line has been created: %s\n", newLine);
//    if(beginRep) free(beginRep);
    return newLine;
}
static int display_info(const char *fpath, const struct stat *sb,  int tflag, struct FTW *ftwbuf){
    if(strcasestr(fpath, fileName)){
        findAndReplaceFile(searchString, replaceString, fpath);
    }
    return 0;
}
/*
 
 */
void findAndReplaceFile(char * string, char * replaceString, const char * fileName){
    FILE * file;
    char * line = NULL;
    size_t len = 0;
    size_t read;
    int newFileSize = 128;
    char * newFile = malloc(sizeof(char) * 128);
    
    file = fopen(fileName, "r");
    if (file == NULL)
        exit(EXIT_FAILURE);
    
//    printf("Length of string: %lu\n", strlen(newFile));
//    printf("Value of length int: %d\n\n", newFileSize);
    
    
    while ((read = getline(&line, &len, file)) != -1) {
//        printf("Retrieved line of length %zu :\n", len);
        printf("%s\n", line);

//        not working  on clang????
//        if(strlen(newFile) < strlen(newFile) + len) realloc(newFile, (newFileSize + len) * 2);
        
        if (strcasestr(line, searchString)) {
            strcat(newFile, Replace(line));
        }else{
            strcat(newFile, line);
        }
    }
    
    puts("\nTHE FOLLOWING IS THE NEW FILE\n----------------------");
    puts(newFile);
    char * newFileName = malloc(sizeof(char) * 256);
    strcpy(newFileName, fileName);
    printf("New file name: %s\n", newFileName);
    remove(fileName);
    
    
    strcpy(newFileName,Replace(newFileName));

    file = fopen(newFileName, "w");
    if (file == NULL)   exit(EXIT_FAILURE);
    printf("New file name: %s\n", newFileName);

    fputs(newFile, file);
    fclose(file);
    
    if (line) free(line);
}
int main(int argc, char *argv[]) {
    int flags = 0;
    
    searchString = argv[1];
    replaceString = argv[2];
    fileName = argv[3];
    
    if (nftw(path, display_info, 20, flags) == -1) { // calls display_info for each file found, including its stat and path!
        perror("files");
        exit(EXIT_FAILURE);
    }

    exit(EXIT_SUCCESS);
}
