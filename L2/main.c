#include <ftw.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/types.h>
#include <dirent.h>

#define aDir "./a/"
#define bDir "./b/"

#define MAX_FILE 100

void synergizeDirs();
void synergizeDirsRec(char * a, char * b);
void findParallelFile(char * abaseDir, char * bBaseDir, char ** aFiles, char ** bFiles, int aSize, int bSize);
int searchFileList(char * item, char ** fileList, int size);
void copyFile(char * src, char * dest);

int main(int argc, char *argv[]){
    
    synergizeDirs();
    return EXIT_SUCCESS;
}

void synergizeDirs(){
    DIR *aDirectory;
    DIR *bDirectory;
    struct dirent *aDe; // directory struct for the a dir
    struct dirent *bDe; // directory struct for the b dir
    int aSize; // Files in the a folder
    int bSize; // Files in the b folder
    int c;     // Counter for while loops
    char ** aFiles = malloc(sizeof(char *) * MAX_FILE); // Paths for the files in the a directory
    char ** bFiles = malloc(sizeof(char *) * MAX_FILE); // Paths for the files in the b directory
    struct stat * test = malloc(sizeof(struct stat));   // stat pointer to be used for statting files
    
    //ensure that the directories are able to be opened
    if (!(aDirectory = opendir(aDir)))  exit(1);
    if (!(bDirectory = opendir(bDir)))  exit(1);
    
    // Allocate all the strings for the files arrays
    for(int i = 0; i < MAX_FILE; ++i){
        aFiles[i] = malloc(sizeof(char) * 128);
        bFiles[i] = malloc(sizeof(char) * 128);
    }

    c = 0; // init the counter to zero
    while (0 != (aDe = readdir(aDirectory))) {
        char * aDirName = malloc(sizeof(char) * 128);
        strcpy(aDirName, aDir);
        if (strcmp(aDe->d_name, "..") && strcmp(aDe->d_name, ".")) {
            strcpy(aFiles[c++], strcat(aDirName, aDe->d_name));
        }
        free(aDirName);
    }
    aSize = c;
    c = 0; // reset the counter to zero
    while (0 != (bDe = readdir(bDirectory))) {
        char * bDirName = malloc(sizeof(char) * 128);
        strcpy(bDirName, bDir);
        if (strcmp(bDe->d_name, "..") && strcmp(bDe->d_name, ".")) {
            strcpy(bFiles[c++], strcat(bDirName, bDe->d_name));
        }
        free(bDirName);
    }
    bSize = c;

    printf("Root A size: %d\n", aSize);
    printf("Root B size: %d\n", bSize);
    
    
    stat(aFiles[4], test);
    printf ("\tThe fifth file in test: %d\n" , S_ISDIR (test->st_mode));
    findParallelFile(aDir, bDir, aFiles, bFiles, aSize, bSize);
}

void findParallelFile(char * abaseDir, char * bBaseDir, char ** aFiles, char ** bFiles, int aSize, int bSize){
    
    for (int i = 0; i < aSize; ++i) printf("A File Found: %s\n" , aFiles[i]);
    for (int i = 0; i < bSize; ++i) printf("B File Found: %s\n" , bFiles[i]);
    struct stat * aStat = malloc(sizeof(struct stat));
    struct stat * bStat = malloc(sizeof(struct stat));
    unsigned long aBaseDirSize = strlen(abaseDir);
    int parallelIndex;
    int aIndex;
    
    
//    char * bPath = malloc(sizeof(char) * 128);
        for(int i = 0; i < aSize; ++i){
            char * fileName = malloc(sizeof(char) * 128);
            char * newBPath = malloc(sizeof(char) * 128);
            strncpy(fileName, (*(aFiles + i) + aBaseDirSize), strlen(aFiles[i]) - aBaseDirSize);
            printf("fileName : %s\n",fileName);
            
            parallelIndex = searchFileList(fileName, bFiles, bSize);
            aIndex = searchFileList(bFiles[parallelIndex], aFiles, aSize);
            printf("Parallel Index: %d\n",parallelIndex);
            if (parallelIndex > -1) {
                puts("parallel index is greater than -1");
                stat(bFiles[parallelIndex], bStat);
                stat(fileName, aStat);
                if (aStat->st_mtime > bStat->st_mtime) {
                    puts("aStat is newer");
                    copyFile(aFiles[aIndex], bFiles[parallelIndex]);
                }
            }
            strcpy(newBPath, bDir);
            strcat(newBPath, fileName);
            printf("new b path is: %s\n",newBPath);
            if(parallelIndex == -1){
                puts("derp");
                copyFile(aFiles[i], newBPath);
            }
            newBPath = "";
        }
}

void copyFile(char * src, char * dest){
    printf("copyFile was called!");
    FILE *source, *target;
    char ch;
    source = fopen(src, "r");
    
    if( source == NULL ){
        printf("Press any key to exit...\n");
        exit(EXIT_FAILURE);
    }

    target = fopen(dest, "w");
    
    if( target == NULL )
    {
        fclose(source);
        printf("Press any key to exit...\n");
        exit(EXIT_FAILURE);
    }
    
    while( ( ch = fgetc(source) ) != EOF )
        fputc(ch, target);
    
    printf("File copied successfully.\n");
    
    fclose(source);
    fclose(target);
}

int searchFileList(char * item, char ** fileList, int size){
    for (int i = 0; i < size; i++) {
        if (!strcmp(item, fileList[i])) {
            return i;
        }
    }
    return -1;
}






