//
//  main.c
//  Lab3
//
//  Created by Matthew D'Amore on 8/7/15.
//  Copyright (c) 2015 derp. All rights reserved.
//

#include <stdio.h>
#include "mongoose.h"


static int ev_handler(struct mg_connection *conn, enum mg_event ev) {
    switch (ev) {
        case MG_REQUEST:
            // /Users/Matt/Desktop/derp.png
            mg_send_file(conn, conn->query_string, NULL);  // Also could be a dir, or CGI
            printf("The query String s: %s\n", conn->request_method);
            puts("a request was made!");
            return MG_MORE; // It is important to return MG_MORE after mg_send_file!
        case MG_AUTH: return MG_TRUE;
        default: return MG_FALSE;
    }
}

int main(int argc, char **argv) {
    char * port = "80";
    if (argc == 3){
        port = argv[2];
    }
    struct mg_server *server = mg_create_server(NULL, ev_handler);
    mg_set_option(server, "listening_port", "80");
    
    printf("Starting on port %s\n", mg_get_option(server, "listening_port"));
    for (;;) mg_poll_server(server, 1000);
    mg_destroy_server(&server);
    
    return 0;
}
