//
//  main.c
//  Lab3Client
//
//  Created by Matthew D'Amore on 8/10/15.
//  Copyright (c) 2015 derp. All rights reserved.
//

#include <stdio.h>
#include "csapp.h"

int main(int argc,  char * argv[]) {
    int port = 8080;
    rio_t rio;
    char * filePath = "/Users/Matt/Desktop/derp.png";
//    if (argc == 3) {
//        filePath = argv[1];
//        port = atoi(argv[2]);
//    }
//    else{
//        printf("invalid arg count!");
//        return EXIT_FAILURE;
//    }
    char * address = malloc(sizeof(char) * 256);
    strcpy(address, "localhost");
//    strcat(address, filePath);
    char buffer[256];
    strcpy(buffer, "GET /test HTTP/1.1\r\n");
    strcat(buffer, "\r\n");
    puts(address);
    int portDesc = Open_clientfd(address, port);
    Rio_readinitb(&rio, portDesc);
    Rio_writen(portDesc, buffer, strlen(buffer));
    ssize_t cnt;
    while ((cnt = Rio_readlineb(&rio,buffer,60)) > 0 ) {
        Rio_writen(STDOUT_FILENO, buffer, cnt);
    }
    
    return EXIT_SUCCESS;
}
