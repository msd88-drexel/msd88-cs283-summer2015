

#include <stdio.h>
#include <pthread.h>

pthread_mutex_t lock;

void *bathroomRun(int * counter){
    pthread_mutex_lock(&lock);
    for(int i = 0; i <1000; ++i){
        
        *counter = *counter + 1;
        printf("Current run value: %d\n", *counter);
        
    }
    pthread_mutex_unlock(&lock);
    return NULL;
}


int main(int argc, const char * argv[]) {

    volatile int bathroomRuns = 0;
    pthread_mutex_init(&lock, NULL);
    pthread_t tid;
    for (int i =0 ; i < 100; ++i){
        pthread_create(&tid, NULL, bathroomRun, &bathroomRuns);
    }
    
    pthread_join(tid, NULL);
    
    return 1;
}
