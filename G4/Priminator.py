#!/usr/bin/python
import sys
from pprint import pprint
import socket
from fractions import gcd
import math
import string

def findCoprimes(primeOne, primeTwo) :

	m = (primeOne-1)*(primeTwo-1)
	c = primeOne * primeTwo

	for i in range(2,m):
		if gcd(m,i) == 1 and gcd(c, i) == 1:
			return i
	return -1

def modInverse(e , m) :
	e = e % m
	for x in range(1 , m) :
		if ( ( (e*x) % m) == 1 ) : return x
	return -1

def is_prime(n):
    for i in range(3, n):
        if n%i == 0:
            return False
    return True

def collectPrimes(limit=1000):
	primes = []
	for i in range(2,limit) :
		if is_prime(i) : primes.append(i)
	return primes	

def encriptString(string, e , c):
	letters = list(string)
	encriptString = ""
	for letter in letters :
		encriptString += str ( ( pow( ord ( letter ) , e ) )  % c ) + ":"
	return encriptString

def decriptString(enString, d, c):	
	thing = ""
	builtWord=enString.split(":")
	for enLetter in builtWord :
		if len(str(enLetter)) > 0 :
			thing += chr( pow (int(enLetter) , d) % c )
	return thing

if __name__ == '__main__':
	pprint(sys.argv)
	p = int(sys.argv[1])
	q = int(sys.argv[2])

	#The limit on the number of primes generated. 
	primes = collectPrimes()

	primeOne = primes[p + 1]
	primeTwo = primes[q + 1]

	c = primeOne * primeTwo
	m = (primeOne-1)*(primeTwo-1)

	e = findCoprimes(primeOne, primeTwo)
	d = modInverse(e, m)

	print("the %dth prime is: %d\nthe %dth prime is: %d\nc is: %d\nm is: %d\ne is: %d\nd is: %d" % (p, primeOne , q , primeTwo, c , m, e, d))

