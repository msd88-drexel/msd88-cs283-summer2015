#!/usr/bin/python
import socket
import sys
import os
from Priminator import decriptString
from Priminator import encriptString
from thread import *
 
# http://www.binarytides.com/python-socket-server-code-example/

HOST = ''   # Symbolic name meaning all available interfaces
PORT = 8889  # Arbitrary non-privileged port

# d=2291
# e=11
# c=2623

c=argv[1]
d=argv[2]
e=argv[3]
PORT=argv[4]

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print 'Socket created'
 
#Bind socket to local host and port
try:
    s.bind((HOST, PORT))
except socket.error as msg:
    print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
    sys.exit()
     
print 'Socket bind complete'
 
#Start listening on socket
s.listen(10)
print 'Socket now listening'
 
#Function for handling connections. This will be used to create threads
def serverthread(conn):
    #Sending message to connected client
    conn.send('Welcome to the server. Type something and hit enter\n') #send only takes string
     
    #infinite loop so that function do not terminate and thread do not end.
    while True:
         
        #Receiving from client
        data = conn.recv(1024)
        print("Data received!!! : %s" % decriptString(data,d,c))
        reply = data
        if not data: 
            break
            s.close()
        conn.sendall(reply)
    #came out of loop
    conn.close()

def chatThread(conn):
    while True:
        data = raw_input()
        conn.send(encriptString(data,e,c))
        if not data:
            print ("blank message entered! exiting...")
            s.close()
            os._exit(1)

#now keep talking with the client
while 1:
    #wait to accept a connection - blocking call
    conn, addr = s.accept()
    print 'Connected with ' + addr[0] + ':' + str(addr[1])
     
    #start new thread takes 1st argument as a function name to be run, second is the tuple of arguments to the function.
    # start_new_thread()
    start_new_thread(serverthread ,(conn,))
    start_new_thread(chatThread ,(conn,))
s.close()