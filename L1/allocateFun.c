//
//  main.c
//  derp
//
//  Created by Matthew D'Amore on 6/25/15.
//  Copyright (c) 20 15 derp. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int get(int array[], int index, int size){
    if(index > size){
        return -1;
    }
    return array[index];
}

void removeElement(int array[], const int * index, int * size){
    //assuming the user knows what they are doing and isnt trying to remove at any weird index...
    int * newArray = malloc(sizeof(int) * *size);
    
    for (int i = 0; i < *index; ++i) {
        newArray[i] = array[i];
    }
    if(*index == *size)
        newArray[*index] = 0;
    else
    {
        newArray[*index] = array[*index + 1];
        for(int i = *index + 1; i < *size; ++i){
            newArray[i] = array[i];
        }
    }
}
void add(int array[], int value, int index, int * currentSize){
    if (index >= *currentSize){
        //we gotta problem
        int * test = (int *)realloc(array, sizeof(int) * (*currentSize * 2) );
        array = test;
        *currentSize = *currentSize *2;
        array[index] = value;
        return;
    }
    array[index] = value;
}

int main(int argc, const char * argv[]) {

    //allocate an array of the size given in an input variable.
    int * theArray = malloc(sizeof(int) * 10);
    int size = 10;
    clock_t begin, end;
    double time_spent;
    
    begin = clock();
    for(int i = 10; i <200; ++i){
        add(theArray, i, i, &size);
    }
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    free(theArray);

    printf("%f",time_spent);
    
}
