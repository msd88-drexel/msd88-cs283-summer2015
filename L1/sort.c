#include <stdio.h>
#include <stdlib.h>

void swap(int array[], int a, int b)
{
    int temp = array[a];           
    array[a] = array[b];      
    array[b] = temp;               
}
void quickSortImplement(int array[], int start, int end)
{
    int i = start;
    int k = end;
    
    //check to make sure at least two elements are being sorted
    if (end - start >= 1)
    {
        int pivot = array[start];
        
        while (k > i)
        {
            while (*(array + i) <= pivot && i <= end)   i++;
            while (*(array + k) > pivot && k >= start)  k--;
            if (k > i)  swap(array, i, k);
        }
        //swap pivot back into the right position
        swap(array, start, k);

        quickSortImplement(array, start, k - 1);
        quickSortImplement(array, k + 1, end);
    }
    else
    {
        return;
    }
}
//wrapper to make the recursive call alittle bit more pretty
void quickSort(int array[], int arraySize)
{
    quickSortImplement(array, 0, arraySize -1);              // quicksort all the elements in the array
}

int main(void) {
    int size = 7;
    int a[] = {8,6,7,5,3,0,9};
    puts("Before: ");
    for(int i = 0; i < size; ++i)
    {
        printf("%d",a[i]);
    }
    puts("\n");
    
    quickSort(a, size);
    
    puts("After: ");
    for(int i = 0; i < size; ++i)
    {
        printf("%d",a[i]);
    }
}