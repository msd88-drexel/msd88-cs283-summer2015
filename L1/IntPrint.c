#include <stdio.h>
#include <stdlib.h>

int main(int argc, const char * argv[]) {
    //Int Stuff
    int * a = malloc(sizeof(int) * 10);
    
    for(int i =0; i < 10; ++i){
        a[i] = i;
        printf("[%d]: %d\n",i,a[i]);
    }
    free (a);
    //Char stuff
    char ** words;
    words = malloc(sizeof(char ) * 15);
    for(int i =0; i < 15; ++i){
        *(words + i) = "this is a word!\0";
    }
    for(int i =0; i < 15; ++i){
        printf("Oh look a word: %s \n", *(words + i));
    }
    free(words);
    
}
