//
//  main.c
//  Assignment 2
//
//  Created by Matthew D'Amore on 7/11/15.
//  Copyright (c) 2015 derp. All rights reserved.
//
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

#define HASH_SIZE 100
#define FILE_PATH "/usr/share/dict/words"

//typedef unsigned long long int int; //Typedef a 64 bit integer for longer words because... why not

struct node {
    char * data;
    struct node * next;
}; typedef struct node node;

struct hashtable {
    node * array[100];
};typedef struct hashtable hashtable;

int bitMask(const char * word);
void hash(hashtable * hash, char * word);
void appendNode(const char * data, node * head, hashtable * table);
void find(hashtable * table, const char * word);
void scrabbleFind(hashtable * table, const char * word, int index, char character);

hashtable * allocateHash();

int main(int argc, const char * argv[]) {
    hashtable * hashTable = allocateHash();
    if(argc < 2){
    	puts("invalid arg count! exiting...");
    	exit(1);
    }
    char word[80];
    strncpy(word, argv[1], strlen(argv[1]));
    hashTable = allocateHash(hashTable);
    if (argv[2]) {
        scrabbleFind(hashTable, argv[2], atoi(argv[4]), *argv[3]);
    }
    else {
        find(hashTable, argv[1]);
    }

    free(hashTable);
    return EXIT_SUCCESS;
}

int bitMask(const char * word){

    int wordIntValue = 0;

    for(int i = 0; i < strlen(word); ++i){
        char characterAtIndex = tolower(word[i]);

        //The word entered was not valid IE didnt contain only letters, so exit.
        if((int)characterAtIndex < 97 || (int)characterAtIndex > 122) continue;

        //Bit mask the individual letter into a power of 2...
        unsigned int characterIntValue = pow ( 2, ( (int)characterAtIndex  % 97) );
        wordIntValue += characterIntValue;

        //        printf("%c: %u\n" , characterAtIndex, characterIntValue );
    }

    return wordIntValue;
}
void hash(hashtable * hash, char * word){

    int hashValue = bitMask(word);
    node * head = hash->array[hashValue % HASH_SIZE];

    appendNode(word, head, hash);

    return;
}
void appendNode(const char * data, node * head, hashtable * table){
    char * newData = (char *)malloc(sizeof(char));
    strncpy(newData, data, strlen(data));

    int tableLocation = bitMask(data) % 100;

    if(head->data == NULL){
        head->data = newData;
        return;
    }
    node * newNode = (node *)malloc(sizeof(node));
    newNode->data = newData;
    newNode->next = head;
    table->array[bitMask(data) %100] = newNode;

}
void find(hashtable * table, const char * word){
    char ** anagrams = malloc(sizeof(char *) * 20);
    int hashTableIndex = bitMask(word) % HASH_SIZE;
    int arrayIndex = 0;
    int arraySize = 20;

    for(node * i = table->array[hashTableIndex]; (i = i->next) ; ){
        if (bitMask(word) == bitMask(i->data) && strlen(word) == strlen(i->data)) {
            anagrams[arrayIndex] = i->data;
            arrayIndex++;
            arraySize++;
        }
        if (arrayIndex == arraySize - 1){
            anagrams = realloc(anagrams, arraySize * 2);
            arraySize *= 2;
        }
    }
    for (int i = 0; i < arraySize; ++i) {
        if(anagrams[i] == NULL) break;
        puts(anagrams[i]);
    }
}
void scrabbleFind(hashtable * table, const char * word, int index, char character){
    char ** anagrams = malloc(sizeof(char *) * 20);
    int hashTableIndex = bitMask(word) % HASH_SIZE;
    int arrayIndex = 0;
    int arraySize = 20;

    for(node * i = table->array[hashTableIndex]; (i = i->next) ; ){
        if (bitMask(word) == bitMask(i->data) && strlen(word) == strlen(i->data) && i->data[index] == character) {
            anagrams[arrayIndex] = i->data;
            arrayIndex++;
            arraySize++;
        }
        if (arrayIndex == arraySize - 1){
            anagrams = realloc(anagrams, arraySize * 2);
            arraySize *= 2;
        }
    }
    for (int i = 0; i < arraySize; ++i) {
        if(anagrams[i] == NULL) break;
        puts(anagrams[i]);
    }

}
hashtable * allocateHash(){
    hashtable * table = malloc(sizeof(hashtable));

    for (int i = 0; i < HASH_SIZE; ++i){
        table->array[i] = (node *)malloc(sizeof(node));
        table->array[i]->next = NULL;
    }

    FILE* dict = fopen(FILE_PATH, "r"); //open the dictionary for read-only access
    if(dict == NULL) {
        exit(EXIT_FAILURE);
    }

    // Read each line of the file, and print it to screen
    char word[128];
    while(fgets(word, sizeof(word), dict) != NULL) {
        long int len = strlen(word);
        if( word[len-1] == '\n' )
            word[len-1] = 0;
        hash(table, word);
    }
    return table;
}
